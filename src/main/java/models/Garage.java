package models;

public class Garage {
    private int id;
    private String nom;
    private String description;
    private String image;
    private String adresseNumber;
    private String addressStreet;
    private String addressCodePostal;
    private String addresseVille;

    public Garage(int id, String nom, String description, String image,
                  String adresseNumber, String addressStreet, String addressCodePostal,
                  String addresseVille) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.image = image;
        this.adresseNumber = adresseNumber;
        this.addressStreet = addressStreet;
        this.addressCodePostal = addressCodePostal;
        this.addresseVille = addresseVille;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAdresseNumber() {
        return adresseNumber;
    }

    public void setAdresseNumber(String adresseNumber) {
        this.adresseNumber = adresseNumber;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getAddressCodePostal() {
        return addressCodePostal;
    }

    public void setAddressCodePostal(String addressCodePostal) {
        this.addressCodePostal = addressCodePostal;
    }

    public String getAddresseVille() {
        return addresseVille;
    }

    public void setAddresseVille(String addresseVille) {
        this.addresseVille = addresseVille;
    }
}
