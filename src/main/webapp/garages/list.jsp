<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Ma premiere vue</title>
</head>

<body>
    <div class="container">
        <h1 class="text-danger text-center">Bienvenue sur ma première vue !</h1>
        <a href="http://localhost:8080/garages/add.jsp">Ajouter un garage</a>
        <div class="row">

            <c:forEach var="garage" items="${garages}">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src=${garage.image}>
                    <div class="card-body">
                        <h5 class="card-title">${garage.nom}</h5>
                        <p class="card-text">${garage.description}</p>
                        <a href="http://localhost:8080/garages/detail.jsp" class="btn btn-primary">Voir le détail</a>
                    </div>
                </div>
            </c:forEach>

                <c:if test="${garages.size() == 0}">
                <h2 class="text-center text-danger"> Aucune voiture ici !</h2>
                </c:if>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>