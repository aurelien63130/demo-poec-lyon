<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Ma premiere vue</title>
</head>

<body>
<div class="container">
    <h1 class="text-danger text-center">Ajoutez un garage</h1>
    <form class="form-control">
        <div class="form-group">
            <label for="nom">Nom du garage</label>
            <input name="nom" id="nom" type="text">
        </div>

        <div class="form-group">
            <label for="description">Description du garage</label>
            <input name="description" id="description" type="text">
        </div>

        <div class="form-group">
            <label for="description">Url de la photo</label>
            <input name="image" id="image" type="text">
        </div>

        <div class="form-group">
            <label for="numRue">Numéro de rue</label>
            <input name="numRue" id="numRue" type="text">
        </div>

        <div class="form-group">
            <label for="nomRue">Nom de la rue</label>
            <input name="nomRue" id="nomRue" type="text">
        </div>


        <div class="form-group">
            <label for="codePostal">Code postal</label>
            <input name="codePostal" id="codePostal" type="text">
        </div>

        <div class="form-group">
            <label for="ville">Ville</label>
            <input name="ville" id="ville" type="text">
        </div>

        <input type="submit" class="btn btn-success">

        <a href="http://localhost:8080/garages/list.jsp">Enfait c'est cool !</a>
    </form>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>